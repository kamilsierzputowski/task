<?php

class Database extends PDO
{
    public function __construct()
    {
        parent::__construct(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME,
                DB_USER, DB_PASS);
    }

    public function select($table, $columnName = null, $parameter = null,
            $rowName = null)
    {
        if ($columnName && $parameter && $rowName) {
            $sth = $this->prepare("SELECT {$rowName} FROM {$table} WHERE {$columnName} = '{$parameter}'");
        } elseif ($columnName && $parameter) {
            $sth = $this->prepare("SELECT * FROM {$table} WHERE {$columnName} = '{$parameter}'");
        } else {
            $sth = $this->prepare("SELECT * FROM {$table}");
        }
        $sth->execute();

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function insert($table, $param = [])
    {
        $columns = $this->prepareColumns($param);
        $values = $this->prepareValues($param);
        $sth = $this->prepare("INSERT INTO {$table} ({$columns}) VALUES ({$values})");
        $sth->execute();
        
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete($table, $parameter, $id)
    {
        $sth = $this->prepare("DELETE FROM {$table} WHERE {$parameter} = {$id}");
        $sth->execute();

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function update($table, $column, $value, $row, $condition)
    {
        if ($column && $value && $row && $condition) {
            $sth = $this->prepare("UPDATE {$table} SET {$column} = '{$value}'
        WHERE {$row} = {$condition}");
        }
        $sth->execute();

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function prepareColumns($parameter = [])
    {
        $columnString = '';
        foreach ($parameter as $key => $value) {
            $columnString .= ', ' . $key;
        }
        return trim(ltrim($columnString, ', '), ', ');
    }

    public function prepareValues($parameter = [])
    {
        $valueString = '';
        foreach ($parameter as $key => $value) {
            $valueString .= ', ' . "'" . $value . "'";
        }
        return ltrim($valueString, ', ');
    }
}
