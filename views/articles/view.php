<form action="Articles/delete" method="post">  
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Title</th>
                <th scope="col">Description</th>
                <th scope="col">Status</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <form action="Articles/deleteArticles" method="post">  
            <?php foreach ($data as $value) : ?>
                <tbody>
                    <tr>
                        <td><?= $value->getId(); ?></td>
                        <td><?= $value->getTitle(); ?></td>
                        <td><?= $value->getDescription(); ?></td>
                        <td><?= $value->getStatus(); ?></td>
                        <td><button type="submit" name="id" value="<?= $value->getId(); ?>" class="btn btn-primary">Delete</button></td>
                    </tr>
                </tbody>
            <?php endforeach; ?>
    </table>
</form>

<div class="conteiner-update" style="width: 50%; margin: auto">
    <h3>Change data</h3>
    <form action="Articles/updateArticles" method="post"> 
        <select class="form-select"  name="id" required >
            <option value="" selected disabled hidden>Choose Id</option>
            <?php foreach ($data as $value) : ?>
                <option value="<?= $value->getId(); ?>"><?= $value->getId(); ?></option>
            <?php endforeach; ?>
        </select>

        <select class="form-select"  name="column" required >
            <option value="" selected disabled hidden>Choose column</option>
            <option value="title">title</option>
            <option value="description">description</option>
            <option value="status">status</option>
        </select>
        <div class="mb-3">
            <label for="exampleInputDescription1" class="form-label">New Content</label>
            <input type="text" class="form-control" id="exampleInputDescription1" name="content" required>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
        