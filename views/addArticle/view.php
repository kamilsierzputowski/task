<style>
    <link rel ="stylesheet" href ="<?= URL; ?>public/css/default.css" />
</style>

<form id="addArticle-form" action="AddArticle/index" method="post">
    <div class="mb-3">
        <label for="exampleInputTitle1" class="form-label">Title</label>
        <input type="text" class="form-control" id="exampleInputTitle1" name="title" required>
    </div>
    <div class="mb-3">
        <label for="exampleInputDescription1" class="form-label">Description</label>
        <input type="text" class="form-control" id="exampleInputDescription1" name="description" required>
    </div>
    <div class="mb-3">
        <select class="form-select"  name="status" required>
            <option value="" selected disabled hidden>Choose quantity</option>
            <option value="available">available</option>
            <option value="not available">not available</option>
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
