<?php

class ArticleModel extends Model
{
    protected const TABLE_NAME_ARTICLE = 'article';
    private $id;
    private $title;
    private $description;
    private $status;
    public function getId()
    {
        return $this->id;
    }

    public function setId(string $id)
    {
        $this->id = $id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    public function insertArticles($param)
    {
        $result = $this->db->insert(self::TABLE_NAME_ARTICLE, $param);
        return $result;
    }

    public function selectArticles()
    {
        $result = $this->db->select(self::TABLE_NAME_ARTICLE);

        foreach ($result as $value) {
            $this->id = $value['id'];
            $this->title = $value['title'];
            $this->description = $value['description'];
            $this->status = $value['status'];
        }
        return $this;
    }

    public function deleteArticle()
    {
        $result = $this->db->delete(self::TABLE_NAME_ARTICLE, 'id', $_POST['id']);
        return $result;
    }

    public function updateArticle()
    {
        $result = $this->db->update(self::TABLE_NAME_ARTICLE, $_POST['column'],
                $value = $_POST['content'], $row = 'id', $_POST['id']);
        return $result;
    }
}
