<?php

require 'models/ArticleModel.php';

class Collection
{
    protected const TABLE_NAME_ARTICLE = 'article';
    protected $articleData = [];
    public function __construct()
    {
        $this->db = new Database();
    }

    public function getArticleData(): array
    {
        return $this->articleData;
    }

    public function setArticleData(array $articleData)
    {
        $this->articleData = $articleData;
    }

    public function getAllArticleData()
    {
        $dbResult = $this->db->select(self::TABLE_NAME_ARTICLE);

        foreach ($dbResult as $row) {
            $article = new ArticleModel();
            $article->setId($row['id']);
            $article->setTitle($row['title']);
            $article->setDescription($row['description']);
            $article->setStatus($row['status']);

            $this->articleData[] = $article;
        }
        return $this;
    }
}
