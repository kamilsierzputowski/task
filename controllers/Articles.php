<?php

require 'models/Article/Collection.php';

class Articles extends Controller
{
    public function index()
    {
        $article = new Collection();

        $showAllArticles = $article->getAllArticleData()->getArticleData();
        $this->view->render('articles/view', $showAllArticles
        );
    }

    public function deleteArticles()
    {
        $article = new ArticleModel();

        $article->deleteArticle();
        header('location: ../Articles');
    }

    public function updateArticles()
    {
        $article = new ArticleModel();

        $article->updateArticle();
        header('location: ../Articles');
    }
}
