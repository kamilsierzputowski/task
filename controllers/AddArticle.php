<?php

require 'models/ArticleModel.php';

class AddArticle extends Controller
{
    public function index()
    {
        if (!empty($_POST['title']) && !empty($_POST['description']) &&
                !empty($_POST['status'])) {
            $addArticle = new ArticleModel();
            
            $addArticle->insertArticles($_POST);
            header('location: ../Articles');
        }
        $this->view->render('addArticle/view', []);
    }
}
