<?php

session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require 'libs/Bootstrap.php';
require 'libs/Controller.php';
require 'libs/Model.php';
require 'libs/View.php';
require 'libs/Session.php';
require 'libs/Database.php';
require 'config/paths.php';
require 'config/database.php';

$app = new Bootstrap();
